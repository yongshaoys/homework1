//
//  ViewController.swift
//  viewPics
//
//  Created by 邵勇 on 2/26/18.
//  Copyright © 2018 邵勇. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var image: UIImageView!
    //let photoCollection = [["A Porsche":"porsche"], ["F35":"f35"], ["Aircraft Carrier":"carrier"]]
    let lblText = ["Porsche", "F35", "Aircraft Carrier"]
    let picTitle = ["porsche", "f35", "carrier"]
    var prevCount : Int = 0
    var nextCount : Int = 3

    
    @IBAction func previous(_ sender: UIButton) {
        prevCount += 1
        nextCount -= 1
        if (prevCount > picTitle.count){
            prevCount -= 1
        } else if (prevCount < 0){
            prevCount = 0
        }
        if (nextCount > picTitle.count){
            nextCount -= 1
        } else if (nextCount < 0){
            nextCount = 0
        }
        print(prevCount, nextCount)
        image.image = UIImage (named: picTitle[prevCount-1])
        label.text = lblText[prevCount-1]
            
        

    }
    @IBAction func next(_ sender: UIButton) {
        prevCount -= 1
        nextCount += 1
        if (prevCount > picTitle.count){
            prevCount -= 1
        } else if (prevCount < 0){
            prevCount = 0
        }
        if (nextCount > picTitle.count){
            nextCount -= 1
        } else if (nextCount < 0){
            nextCount = 0
        }
        print(prevCount, nextCount)
        image.image = UIImage (named: picTitle[nextCount-1])
        label.text = lblText[nextCount-1]
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

